package com.fabszn.gplus.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * .
 *
 * @author fsznajderman
 *         date :  15/06/13
 */
@DatabaseTable(tableName = "CONFIGURATION")
public class Configuration {
    private static final String INIT_SYNCHRO_DEFAULT_VALUE = "false";
    public  static final long ID_CONFIGURATION = 1l;
    @DatabaseField(generatedId = true)
    private long id;

    @DatabaseField(defaultValue = INIT_SYNCHRO_DEFAULT_VALUE)
    private boolean synchroInit = Boolean.FALSE;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isSynchroInit() {
        return synchroInit;
    }

    public void setSynchroInit(boolean synchroInit) {
        this.synchroInit = synchroInit;
    }
}
