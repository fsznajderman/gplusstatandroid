package com.fabszn.gplus.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * .
 *
 * @author fsznajderman
 *         date :  27/05/13
 */
@DatabaseTable(tableName = "ARTICLE")
public class Article {


    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField
    private String title;
    @DatabaseField
    private String plusone;
    @DatabaseField
    private String share;
    @DatabaseField
    private long publicationDate;
    @DatabaseField
    private String content;
    @DatabaseField
    private String googleId;


    public void setPlusone(String plusone) {
        this.plusone = plusone;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlusone() {
        return plusone;
    }

    public String getShare() {
        return share;
    }

    public String getTitle() {
        return title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(long publicationDate) {
        this.publicationDate = publicationDate;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }


    public String getGoogleId() {
        return googleId;
    }

    public void setGoogleId(String googleId) {
        this.googleId = googleId;
    }
}
