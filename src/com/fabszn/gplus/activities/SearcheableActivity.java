package com.fabszn.gplus.activities;



import android.app.ListActivity;
import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.fabszn.gplus.R;
import com.fabszn.gplus.activities.adapter.ArticleListAdapter;
import com.fabszn.gplus.model.Article;
import com.google.common.collect.Lists;

/**
 * .
 *
 * @author fsznajderman
 *         date :  22/06/13
 */
public class SearcheableActivity extends ListActivity {


    protected static final String FONTS_BOLD = "fonts/Essence_Sans_Bold.ttf";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        String query=null;
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            query = intent.getStringExtra(SearchManager.QUERY);
        }
        Toast.makeText(this,query,3);


        Article a = new Article();
        a.setTitle(query);
        setListAdapter(new ArticleListAdapter(this, Lists.newArrayList(a), Typeface.createFromAsset(getAssets(), FONTS_BOLD)));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_result,menu);

        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        finish();


        return super.onMenuItemSelected(featureId, item);
    }
}
