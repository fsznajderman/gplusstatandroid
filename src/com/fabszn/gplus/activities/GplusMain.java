package com.fabszn.gplus.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import com.fabszn.gplus.R;
import com.fabszn.gplus.activities.adapter.ArticleListAdapter;
import com.fabszn.gplus.db.BddManager;
import com.fabszn.gplus.model.Article;
import com.fabszn.gplus.utils.ArticleHttpLoader;
import ru.korniltsev.flymenu.DoubleSideFlyInMenuLayout;

import java.util.List;

public class GplusMain extends BaseActivity {
    /**
     * Called when the activity is first created.
     */

    private DoubleSideFlyInMenuLayout mMenu;
    public static final int SIDE_BAR_ID = 666;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);


        ListView lvListe = (ListView) findViewById(R.id.listarticles);


        lvListe.setAdapter(new ArticleListAdapter(this, getDatas(), face));


        setMenuView(R.layout.menu);
        mMenu = new DoubleSideFlyInMenuLayout(this);

        mMenu.setId(SIDE_BAR_ID);
        setUpSliding(mMenu);
        mMenu.setMenuWidth(ViewGroup.LayoutParams.WRAP_CONTENT);


        if (savedInstanceState != null && savedInstanceState.getBoolean("opened", false)) {
            setMenuOpened();
        }

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("opened", false);
    }


    private List<Article> getDatas() {
        Toast t = Toast.makeText(getApplicationContext(), "LoadLoadLoadLoadLoadLoad",
                50);
        t.show();

        final BddManager bdd = BddManager.getInstance(this);

        Log.d("GPLUSTAT", "OPEN");

        try {
            List<Article> articles = (new ArticleHttpLoader(bdd.loadAll()).execute()).get();

            for (Article a : articles) {
                bdd.addNewArticle(a);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


        return bdd.loadAll();
    }


}
