package com.fabszn.gplus.activities;


import android.app.ActionBar;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;
import com.fabszn.gplus.R;
import com.fabszn.gplus.db.BddManager;
import com.fabszn.gplus.model.Article;

import static com.fabszn.gplus.utils.ParamIdentifier.ID_ARTICLE_PARAM;

/**
 * .
 *
 * @author fsznajderman
 *         date :  12/06/13
 */
public class DisplayArticle extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.displayarticle);


        Bundle objetbunble = this.getIntent().getExtras();

        Long idArticle = null;
        if (objetbunble != null && objetbunble.containsKey(ID_ARTICLE_PARAM)) {
            idArticle = Long.parseLong((String) objetbunble.get(ID_ARTICLE_PARAM));
        }


        final Article article = BddManager.getInstance(this).findArticlebyId(idArticle);

        final TextView txv = ((TextView) findViewById(R.id.lastArticle));
        txv.setMovementMethod(new ScrollingMovementMethod());
        ((TextView) findViewById(R.id.lastArticle)).setText(Html.fromHtml(article.getContent()));
        ActionBar actionBar = getActionBar();
        actionBar.setTitle(article.getTitle());
    }
}
