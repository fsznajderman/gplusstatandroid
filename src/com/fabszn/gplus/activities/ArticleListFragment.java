package com.fabszn.gplus.activities;

import android.app.Fragment;
import android.app.ListFragment;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fabszn.gplus.R;
import com.fabszn.gplus.activities.adapter.ArticleListAdapter;
import com.fabszn.gplus.db.BddManager;
import com.fabszn.gplus.model.Article;
import com.fabszn.gplus.utils.ArticleHttpLoader;

import java.util.List;

/**
 * .
 *
 * @author fsznajderman
 *         date :  26/07/13
 */
public class ArticleListFragment extends Fragment {

    protected static final String FONTS_BOLD = "fonts/Essence_Sans_Bold.ttf";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {




        //setListAdapter(new ArticleListAdapter(inflater.getContext(), getDatas(inflater.getContext()), Typeface.createFromAsset(inflater.getContext().getAssets(), FONTS_BOLD)));
        return inflater.inflate(R.layout.listarticles,container);
    }

    private List<Article> getDatas(final Context context) {

            final BddManager bdd = BddManager.getInstance(context);

            try {
                List<Article> articles = (new ArticleHttpLoader(bdd.loadAll()).execute()).get();

                for (Article a : articles) {
                    bdd.addNewArticle(a);
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            return bdd.loadAll();
        }
}
