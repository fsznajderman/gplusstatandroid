package com.fabszn.gplus.activities.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.fabszn.gplus.R;
import com.fabszn.gplus.activities.DisplayArticle;
import com.fabszn.gplus.model.Article;

import java.util.List;

import static com.fabszn.gplus.utils.ParamIdentifier.ID_ARTICLE_PARAM;

/**
 * .
 *
 * @author fsznajderman
 *         date :  28/05/13
 */
public class ArticleListAdapter extends BaseAdapter {


    private LayoutInflater inflater;
    private List<Article> articles;
    private Typeface face;


    public ArticleListAdapter(final Context context, final List<Article> articles, final Typeface face) {
        this.articles = articles;
        this.inflater = LayoutInflater.from(context);
        this.face = face;
    }

    @Override
    public int getCount() {
        return articles.size();
    }

    @Override
    public Article getItem(int i) {
        return articles.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {


        view = inflater.inflate(R.layout.itemarticle, null);

        TextView title = (TextView) view.findViewById(R.id.articleTitle);
        TextView plusone = (TextView) view.findViewById(R.id.articlePlusone);
        TextView share = (TextView) view.findViewById(R.id.articleShare);
        TextView idArticle = (TextView) view.findViewById(R.id.idarticle);

        title.setText(getItem(i).getTitle());
        plusone.setText(getItem(i).getPlusone());
        share.setText(getItem(i).getShare());
        idArticle.setText("" + getItem(i).getId());
        title.setTypeface(face);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView idArticle = (TextView) view.findViewById(R.id.idarticle);
                final Bundle objetbunble = new Bundle();
                objetbunble.putString(ID_ARTICLE_PARAM, "" + idArticle.getText().toString());

                Intent i = new Intent(view.getContext(), DisplayArticle.class);
                i.putExtras(objetbunble);
                view.getContext().startActivity(i);

            }
        });


        return view;
    }
}
