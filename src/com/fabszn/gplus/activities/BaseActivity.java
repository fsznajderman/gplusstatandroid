package com.fabszn.gplus.activities;


import android.app.Activity;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;
import com.fabszn.gplus.R;
import com.fabszn.gplus.db.BddManager;
import com.fabszn.gplus.utils.ParamIdentifier;
import ru.korniltsev.flymenu.DoubleSideFlyInMenuLayout;

/**
 * .
 *
 * @author fsznajderman
 *         date :  08/06/13
 */
public class BaseActivity extends Activity {

    protected static final String FONTS_BOLD = "fonts/Essence_Sans_Bold.ttf";

    private View menu;
    private DoubleSideFlyInMenuLayout container;
    protected Typeface face;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        face = Typeface.createFromAsset(getAssets(), FONTS_BOLD);
    }

    public final void setMenuView(final int resId) {
        menu = getLayoutInflater().inflate(resId, null);

    }


    public final void setUpSliding(final DoubleSideFlyInMenuLayout l) {
        container = l;
        ViewGroup decor = (ViewGroup) getWindow().getDecorView();
        ViewGroup decorChild = (ViewGroup) decor.getChildAt(0);
        decor.removeView(decorChild);

        container.addView(menu);
        container.addView(decorChild);
        menu.setTag("menu");
        decorChild.setTag("host");

        decor.addView(container, new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
        ));

        TypedArray a = getTheme().obtainStyledAttributes(new int[]{android.R.attr.windowBackground});
        int background = a.getResourceId(0, 0);
        decorChild.setBackgroundResource(background);
        a.recycle();
        ((TextView) findViewById(R.id.parametres)).setTypeface(face);
        ((TextView) findViewById(R.id.allArticlesId)).setTypeface(face);
        ((TextView) findViewById(R.id.lastArticleId)).setTypeface(face);
        ((TextView) findViewById(R.id.statsId)).setTypeface(face);

        findViewById(R.id.lastArticleId).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                final Bundle objetbunble = new Bundle();
                objetbunble.putString(ParamIdentifier.ID_ARTICLE_PARAM, "" + BddManager.getInstance(view.getContext()).getLastArticle().getId());

                Intent i = new Intent(getApplicationContext(), DisplayArticle.class);
                i.putExtras(objetbunble);
                container.toggle();
                startActivity(i);

            }
        });

    }


    final public void setMenuOpened() {
        container.setOpenedOnStart();

    }

    public DoubleSideFlyInMenuLayout getContainer() {
        return container;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);


        // SearchView
        MenuItem itemSearch = menu.findItem(R.id.menu_search);


        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        SearchView mSearchView = (SearchView) itemSearch.getActionView();
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(new ComponentName(this,SearcheableActivity.class)));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_about:
                // Comportement du bouton "A Propos"
                return true;
            case R.id.menu_refresh:
                // Comportement du bouton "Rafraichir"
                return true;
            case R.id.menu_search:
                // Comportement du bouton "Recherche"
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}