package com.fabszn.gplus.db;

import android.content.Context;
import com.fabszn.gplus.model.Article;
import com.fabszn.gplus.model.Configuration;
import com.google.common.base.Function;
import com.google.common.collect.Ordering;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * .
 *
 * @author fsznajderman
 *         date :  31/05/13
 */
public class BddManager {

    private DatabaseAccess bdd;
    private static BddManager bddManager = null;

    public static BddManager getInstance(Context context) {

        if (bddManager == null) {
            bddManager = new BddManager(context);
        }

        return bddManager;
    }

    private BddManager(Context context) {
        bdd = new DatabaseAccess(context);
    }


    public void addNewArticle(final Article article) {

        try {
            bdd.getDao(Article.class).create(article);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Article> loadAll() {
        try {
            final List<Article> l = bdd.getDao(Article.class).queryForAll();

            Collections.sort(l, Ordering.natural().reverse().onResultOf(new Function<Article, Long>() {
                @Override
                public Long apply(Article o) {
                    return o.getPublicationDate();
                }
            }));

            return l;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Article getLastArticle() {
        try {
            final List<Article> l = bdd.getDao(Article.class).queryForAll();

            Collections.sort(l, Ordering.natural().reverse().onResultOf(new Function<Article, Long>() {
                @Override
                public Long apply(Article o) {
                    return o.getPublicationDate();
                }
            }));

            return l.iterator().next();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public Article findArticlebyId(final Long id) {
        try {
            Dao<Article, Long> dao = bdd.getDao(Article.class);
            return dao.queryForId(id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public Configuration getConfiguration() {
        try {
            Dao<Configuration, Long> dao = bdd.getDao(Configuration.class);

            if (dao.queryForAll().isEmpty()) {
                final Configuration configuration = new Configuration();
                configuration.setId(Configuration.ID_CONFIGURATION);
                dao.create(configuration);
            }

            return dao.queryForId(Configuration.ID_CONFIGURATION);
        } catch (SQLException e) {
            throw new RuntimeException("Error during configuration loading " + e);
        }

    }


    public void updateConfiguration(final Configuration configuration) {
        try {
            Dao<Configuration, Long> dao = bdd.getDao(Configuration.class);
            dao.update(configuration);
        } catch (SQLException e) {
            throw new RuntimeException("Error during configuration update : " + e);
        }

    }

}



