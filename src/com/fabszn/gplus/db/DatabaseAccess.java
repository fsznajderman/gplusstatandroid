package com.fabszn.gplus.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.fabszn.gplus.model.Article;
import com.fabszn.gplus.model.Configuration;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * .
 *
 * @author fsznajderman
 *         date :  30/05/13
 */
public class DatabaseAccess extends OrmLiteSqliteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    //TODO Remplacer par annotation

    //TODO externaliser
    public static final String DATABASE_NAME = "gplusstatdb";

    public DatabaseAccess(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, Article.class);
            TableUtils.createTable(connectionSource, Configuration.class);




        } catch (SQLException e) {
            throw new RuntimeException("Error during database Creation" + e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i2) {
        try {
            TableUtils.dropTable(connectionSource, Article.class, Boolean.TRUE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

