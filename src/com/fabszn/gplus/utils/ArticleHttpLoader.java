package com.fabszn.gplus.utils;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.JsonReader;
import com.fabszn.gplus.model.Article;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.collect.Collections2;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * .
 *
 * @author fsznajderman
 *         date :  27/05/13
 */
public class ArticleHttpLoader extends AsyncTask<Void, Void, List<Article>> {

    public static final String TITLE = "title";
    public static final String SHARED = "shared";
    public static final String PLUS_ONE = "plusOne";
    public static final String PUBLICATION_DATE = "publicationDate";
    public static final String CONTENT = "content";
    public static final String GOOGLE_ID = "googleId";
    public static final String HTTP_GPLUSSTATS_FSZNAJDERMAN_FR_SYNCHRO = "http://gplusstats.fsznajderman.fr/synchro/";
    public static final String NONE = "none";
    public static final int START = 0;
    public static final int END = 9;

    private List<Article> existingArticle;



    public ArticleHttpLoader(final List<Article> articles) {
        existingArticle = articles;

    }


    @Override
    protected void onPreExecute() {



    }

    @Override
    protected void onPostExecute(List<Article> articles) {



    }

    @Override
    protected List<Article> doInBackground(Void... voids) {

        AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
        final String queryParam = Joiner.on('-').join(Collections2.transform(existingArticle, new Function<Article, String>() {
            @Override
            public String apply(Article article) {
                return article.getGoogleId().substring(START, END);
            }
        }));

        final List<Article> messages = new ArrayList<Article>();
        try {
            HttpResponse res = client.execute(new HttpGet(HTTP_GPLUSSTATS_FSZNAJDERMAN_FR_SYNCHRO + (queryParam.isEmpty() ? NONE : queryParam)));

            InputStreamReader bip = new InputStreamReader(res.getEntity().getContent());

            JsonReader reader = new JsonReader(bip);

            reader.beginArray();
            while (reader.hasNext()) {
                messages.add(getArticleFromJson(reader));
            }
            reader.endArray();


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            client.close();
        }

        return messages;
    }

    private Article getArticleFromJson(JsonReader reader) throws IOException {
        Article art = new Article();
        reader.beginObject();
        while (reader.hasNext()) {

            String name = reader.nextName();
            if (name.equals(TITLE)) {
                art.setTitle(reader.nextString());
            } else if (name.equals(SHARED)) {
                art.setShare(reader.nextString());
            } else if (name.equals(PLUS_ONE)) {
                art.setPlusone(reader.nextString());
            } else if (name.equals(PUBLICATION_DATE)) {
                art.setPublicationDate(reader.nextLong());
            } else if (name.equals(CONTENT)) {
                art.setContent(reader.nextString());
            } else if (name.equals(GOOGLE_ID)) {
                art.setGoogleId(reader.nextString());
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();

        return art;
    }
}
